#!/bin/bash

github="https://www.github.com/"
vim_plugins=("ctrlpvim/ctrlp.vim" "mattn/emmet-vim" "tpope/vim-surround" "tpope/vim-commentary" "Raimondi/delimitMate" "w0rp/ale" "othree/html5.vim" "octol/vim-cpp-enhanced-highlight" "lifepillar/vim-solarized8" "nanotech/jellybeans.vim" "noahfrederick/vim-hemisu")

pushd $(dirname $0) # Change to 'dotfiles' directory

# Look at me being all functional
for config in $(ls | grep -v $(basename $0))
do
    ln -sf $(pwd)/$config $HOME/.$config
done

popd

# Setup vim packages
mkdir -p ~/.vim/{swap,backup}
mkdir -p ~/.vim/pack/{other,shimpjn}/{start,opt}

pushd ~/.vim/pack/other/start

for plugin in ${vim_plugins[@]}
do
    git clone $github$plugin
done

popd
